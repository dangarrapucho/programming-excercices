#include <iostream>
using namespace std;
//P29212 - Modular Exponentiation 
typedef unsigned long long LL;

LL expMod(LL n, LL k, LL m) {
	if (k == 0) return 1;
	else if (k == 1) return n % m; 
	else {
		LL p = k / 2;
		LL aux = expMod(n, p, m);
		if (k % 2 == 0) return ((aux % m) * (aux % m)) % m;
		else return ((aux % m) * (aux % m) * (n % m)) % m;

	}
}
int main()
{
	LL n, k, m;
	while (cin >> n >> k >> m) {
		LL res = expMod(n, k, m);
		cout << res << endl;

	}
}

